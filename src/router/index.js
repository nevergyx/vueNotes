import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import TreeSelectTest from "@/components/selectTree/treeSelectTest";
import HoverMaskTest from "../components/imgHoverMask/HoverMaskTest";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/treeSelectTest',
      name:'treeSelectTest',
      component: TreeSelectTest
    },
    {
      path: '/hoverMaskTest',
      name:'hoverMaskTest',
      component: HoverMaskTest
    }
  ]
})
